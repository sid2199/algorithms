'''
    Biotonic array: array in which the elements are in increasing order followed by decreasing order.
'''

from random import randint

class BiotonicArray():
    def __init__(self, n: int):
        self.arr = self.prepare_biotonic_array(n)

    def prepare_biotonic_array(self, n: int):
        self.arr = list({randint(0, 100) for _ in range(n)})
        partition = sum([randint(0,n//2) for _ in range(10)])//10
        self.arr = self.arr[:partition] + self.arr[:partition:-1]
        return self.arr

    def find_max_index(self):
        l = 0
        r = len(self.arr)-1
        while l <= r:
            mid = (l+r)//2
            # print(f"{l}->{self.arr[l]} | {mid}->{self.arr[mid]} | {r}->{self.arr[r]}")
            if l == r:
                return l
            if l+1 == r:
                if self.arr[l] > self.arr[r]:
                    return l
                return r

            # print(f"[{self.arr[mid-1]}] [{self.arr[mid]}] [{self.arr[mid+1]}]")
            if self.arr[mid-1] < self.arr[mid] and self.arr[mid+1] < self.arr[mid]:
                return mid

            if self.arr[mid] > self.arr[mid+1] and self.arr[mid] < self.arr[mid-1]:
                r = mid - 1
            else:
                l = mid+1
        return -1

    def ascending_binary_search(self, target: int, l:int, r: int):
        while l <= r:
            mid = (l+r)//2
            if self.arr[mid] == target:
                return mid

            if target < self.arr[mid]:
                r = mid - 1
            else:
                l = mid + 1
        return -1

    def descending_binary_search(self, target: int, l:int, r: int):
        while l <= r:
            mid = (l+r)//2
            if self.arr[mid] == target:
                return mid

            if target > self.arr[mid]:
                r = mid - 1
            else:
                l = mid + 1
        return -1

    def find(self, target: int):
        biotonic_point = self.find_max_index()
        if self.arr[biotonic_point] == target:
            return biotonic_point
        res = self.ascending_binary_search(target, 0, biotonic_point-1)
        if res != -1:
            return res
        return self.descending_binary_search(target, biotonic_point+1, len(self.arr)-1)


if __name__ == "__main__":
    n=101
    targets = [randint(0, 100) for _ in range(n)]
    for target in targets:
        obj = BiotonicArray(50)
        rv = obj.find(target)
        actual_value = obj.arr.index(target) if target in obj.arr else -1
        assert rv == actual_value, f"\n{target=}\n{rv=}\n{actual_value=}\n{obj.arr=}"
        print("[PASS]")

'''
    3 SUM
'''
from time import perf_counter
from random import randint
from typing import Tuple


def time_execution(func):
    def inner(*args, **kwargs):
        start = perf_counter()
        rv = func(*args, **kwargs)
        return rv, perf_counter() - start
    return inner


class ThreeSum():
    def __init__(self, arr: list[int]) -> None:
        self.arr = arr

    @time_execution
    def brute_force(self, target: int) -> Tuple[int, int, int]:
        n = len(self.arr)
        for i in range(n-2):
            for j in range(i+1, n-1):
                for k in range(j+1, n):
                    if self.arr[i] + self.arr[j] + self.arr[k] == target:
                        return self.arr[i], self.arr[j], self.arr[k]
        return -1, -1, -1

    @time_execution
    def two_pointer(self, target: int) -> Tuple[int, int, int]:
        n = len(self.arr)
        self.arr.sort()
        for p0 in range(0, n-2):
            p1, p2 = p0+1, n-1
            _target = target - self.arr[p0]
            if _target <= self.arr[p0]:
                break
            while p1 < p2:
                if _target > self.arr[p1] + self.arr[p2]:
                    p1 += 1
                elif _target < self.arr[p1] + self.arr[p2]:
                    p2 -= 1
                else:
                    return self.arr[p0], self.arr[p1], self.arr[p2]
        return -1, -1, -1



if __name__ == "__main__":
    n = 20000
    arr = [randint(0, 100) for _ in range(n)]
    targets = [randint(0, 100) for _ in range(10)]

    for target in targets:
        obj = ThreeSum(arr)
        brute_force_result, brute_force_time = obj.brute_force(target)
        two_pointer_result, two_pointer_time = obj.two_pointer(target)

        print(f'''
-----------------------------------------------------------------------------------------
{target=}
{brute_force_result=} | {two_pointer_result=} | {sum(brute_force_result)==sum(two_pointer_result)}
{brute_force_time=}
{two_pointer_time=}
Winner: {"brute force" if brute_force_time<two_pointer_time else "2 pointer"}
-----------------------------------------------------------------------------------------
''', flush=True)

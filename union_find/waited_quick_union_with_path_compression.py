'''
    Waited Quick Union with Path compression
'''

from quick_union_with_path_compression import QuickUnionWithPathCompression
from waited_quick_union import WaitedQuickUnion

class WaitedQuickUnionWithPathCompression(QuickUnionWithPathCompression, WaitedQuickUnion):
    def __init__(self, n):
        '''
            Waited Quick Union
            :param: n : Nodes count
        '''
        WaitedQuickUnion.__init__(self, n)

    def root(self, nid: int) -> int:
        '''
            connect query: return whether 2 nodes are connected or not
            :param: nid: node id
            time complexity: O(n)
        '''
        return QuickUnionWithPathCompression.root(self, nid)
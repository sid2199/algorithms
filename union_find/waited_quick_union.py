'''
    Waited Quick Union
'''

from quick_union import QuickUnion


class WaitedQuickUnion(QuickUnion):
    def __init__(self, n: int) -> None:
        '''
            Waited Quick Union
            :param: n : Nodes count
        '''
        QuickUnion.__init__(self, n)
        self.nodes_size = [1 for _ in range(n)]

    def union(self, p: int, q: int):
        '''
            union command: connects 2 nodes
            :param: p: node id
            :param: q: node id
            time complexity: O(n)
        '''
        pid = self.root(p)
        qid = self.root(q)
        if pid != qid:
            if self.nodes_size[pid] < self.nodes_size[qid]:
                self.nodes_size[pid] += self.nodes_size[qid]
                self.nodes[qid] = pid
            else:
                self.nodes_size[qid] += self.nodes_size[pid]
                self.nodes[pid] = qid
            return "Connected"
        return "Already connected"


def main():
    wqu = WaitedQuickUnion(100)
    print(wqu.union(4, 3))
    print(wqu.union(3, 8))
    print(wqu.union(6, 5))
    print(wqu.union(9, 4))
    print(wqu.union(2, 1))
    print(wqu.connected(0, 7))
    print(wqu.connected(8, 9))
    print(wqu.union(5, 0))
    print(wqu.union(7, 2))
    print(wqu.union(6, 1))
    print(wqu.union(1, 0))
    print(wqu.connected(0, 7))

if __name__ == "__main__":
    main()

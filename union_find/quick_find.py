'''
    Quick find [eager approach]
'''

# from typing import 
# from testcases import *


class QuickFind():
    '''
                        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        union(5, 0)     [0, 1, 2, 3, 4, 0, 6, 7, 8, 9]
    '''
    def __init__(self, n: int) -> None:
        '''
            Quick Find [eager approach]
            :param: n : Nodes count
        '''
        self.nodes = [i for i in range(n)]

    def union(self, p: int, q: int):
        '''
            union command: connects 2 nodes
            :param: p: node id
            :param: q: node id
            time complexity: O(n)
        '''
        if not self.connected(p, q):
            pid = self.nodes[p]
            qid = self.nodes[q]
            self.nodes[p] = qid
            for i in range(len(self.nodes)):
                # check if node was previously connected to another nodes
                # marking them all as connected
                # this loop every time takes O(n)
                if self.nodes[i] == pid:
                    self.nodes[i] = qid
            return "Connected"
        return "Already connected"

    def connected(self, p: int, q: int) -> bool:
        '''
            connect query: return whether 2 nodes are connected or not
            :param: p: node id
            :param: q: node id
            time complexity: O(1)
        '''
        return self.nodes[p] == self.nodes[q]


def main():
    qf = QuickFind(100)
    print(qf.union(4, 3))
    print(qf.union(3, 8))
    print(qf.union(6, 5))
    print(qf.union(9, 4))
    print(qf.union(2, 1))
    print(qf.connected(0, 7))
    print(qf.connected(8, 9))
    print(qf.union(5, 0))
    print(qf.union(7, 2))
    print(qf.union(6, 1))
    print(qf.union(1, 0))
    print(qf.connected(0, 7))

if __name__ == "__main__":
    main()

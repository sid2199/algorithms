'''
    quick-union [lazy approach]
'''

class QuickUnion():
    def __init__(self, n: int) -> None:
        '''
            Quick Union [eager approach]
            :param: n : Nodes count
        '''
        self.nodes = [i for i in range(n)]

    def union(self, p: int, q: int):
        '''
            union command: connects 2 nodes
            :param: p: node id
            :param: q: node id
            time complexity: O(n)
        '''
        pid = self.root(p)
        qid = self.root(q)
        if pid != qid:
            self.nodes[qid] = pid
            return "Connected"
        return "Already connected"

    def root(self, nid: int) -> int:
        '''
            connect query: return whether 2 nodes are connected or not
            :param: nid: node id
            time complexity: O(n)
        '''
        while nid != self.nodes[nid]:
            nid = self.nodes[nid]
        return nid

    def connected(self, p: int, q: int) -> bool:
        '''
            connect query: return whether 2 nodes are connected or not
            :param: p: node id
            :param: q: node id
            time complexity: O(n)
        '''
        return self.root(p) == self.root(q)


def main():
    qf = QuickUnion(100)
    print(qf.union(4, 3))
    print(qf.union(3, 8))
    print(qf.union(6, 5))
    print(qf.union(9, 4))
    print(qf.union(2, 1))
    print(qf.connected(0, 7))
    print(qf.connected(8, 9))
    print(qf.union(5, 0))
    print(qf.union(7, 2))
    print(qf.union(6, 1))
    print(qf.union(1, 0))
    print(qf.connected(0, 7))

if __name__ == "__main__":
    main()
'''
    Quick Union with Path Compression
'''

from quick_union import QuickUnion

class QuickUnionWithPathCompression(QuickUnion):
    def __init__(self, n: int) -> None:
        '''
            Quick Union with Path Compression
            :param: n : Nodes count
        '''
        QuickUnion.__init__(self, n)

    def root(self, nid: int) -> int:
        '''
            connect query: return whether 2 nodes are connected or not
            :param: nid: node id
            time complexity: O(n)
        '''
        root = nid
        while root != self.nodes[root]:
            root = self.nodes[root]

        # Flattening the tree
        # pointing every node in the path to the root
        while nid != self.nodes[nid]:
            nid = self.nodes[nid]
            self.nodes[nid] = root
        return root



def main():
    qupc = QuickUnionWithPathCompression(100)
    print(qupc.union(4, 3))
    print(qupc.union(3, 8))
    print(qupc.union(6, 5))
    print(qupc.union(9, 4))
    print(qupc.union(2, 1))
    print(qupc.connected(0, 7))
    print(qupc.connected(8, 9))
    print(qupc.union(5, 0))
    print(qupc.union(7, 2))
    print(qupc.union(6, 1))
    print(qupc.union(1, 0))
    print(qupc.connected(0, 7))

if __name__ == "__main__":
    main()
